# Ultimate Eligibility

Detect inactive users and potential guest users; and demote them.

This will help you see how much Ultimate may cost you.

#### How To Use

1. Clone or fork repository into your own GitLab Instance
1. Go to `CI/CD` > `Pipelines` > `Run Pipeline` ([example](https://gitlab.com/poffey21/ultimate-eligibility/-/pipelines/new))
1. Provide a GitLab Instance via the `GITLAB_HOST` and an admin token via `PRIVATE_TOKEN`
1. Voila! The [job](https://gitlab.com/poffey21/ultimate-eligibility/-/jobs/1042999933) and resulting report will tell you how many licenses you may need to purchase if you went to ultimate.
1. IF you would like to also demote users to the Guest Role across the projects in which they're assigned, you can do so by running the pipeline and setting the `ALTER` variable to `True`.  This will create a Manual Job in the second stage that you can run which will take the list of Guest-Eligible Users and demote them to Guest Roles.
