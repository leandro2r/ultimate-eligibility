from envparse import Env
from gitlab import Gitlab as GitLab
import json
import os
import sys

DEBUG_COUNT = 50


env = Env()
if os.path.isfile('.env'):
    env.read_envfile('.env')


def main():
    sys.stdout.write('Running\n')
    project_url = env("CI_PROJECT_URL")
    private_token = env('PRIVATE_TOKEN')
    origin = env('GITLAB_HOST')
    ignored_users = env('IGNORED_USERS', cast=list, default=[])
    max_actions_considered = env('MAX_ACTIONS_CONSIDERED', cast=int, default=1000)
    top_level_namespace_id = env('TOP_LEVEL_NAMESPACE_ID', cast=int, default=0)
    debug = env('DEBUG', cast=bool, default=False)
    gl = GitLab(url=origin, private_token=private_token)
    gl.auth()
    sys.stdout.write('Authenticated\n')
    sys.stdout.flush()
    
    sys.stdout.write(f'{project_url}/-/pipelines/new \n')

    all_project_ids_in_namespace = []

    if top_level_namespace_id != 0:
        top_level_namespace = gl.groups.get(top_level_namespace_id)
        sys.stdout.write('*** ONLY RUNNING FOR A SINGLE TOP-LEVEL NAMESPACE ***\n')
        sys.stdout.write(str(top_level_namespace_id) + '\n\n')
        users = top_level_namespace.billable_members.list(all=True, active=True, without_project_bots=True)
        all_project_ids_in_namespace = [x.id for x in top_level_namespace.projects.list(all=True)]
    else:
        sys.stdout.write('*** RUNNING FOR A COMPLETE GITLAB INSTANCE ***\n')
        users = gl.users.list(all=True, active=True, without_project_bots=True)
    sys.stdout.flush()

    # See a full list of events at https://docs.gitlab.com/ee/api/events.html
    reporter_plus_events = ['approved', 'pushed to', 'pushed new', 'imported', 'accepted', 'merged']

    all_event_actions = set()
    eligible_for_removal = []
    eligible_for_removal_id = []
    eligible_for_guest = []
    eligible_for_guest_id = []
    eligible_for_ignorance = []
    user_action_counts = []
    private_project_caveats = 0
    count = 0
    sys.stdout.write(f'Running scan for {len(users)} users.\n')
    sys.stdout.write(f"Users with no events will be counted for potentials for removal.\n")
    sys.stdout.write(f'Users with no events in the following list will be counted for potentials for guest access only.\n')
    sys.stdout.write(f' - ' + ', '.join(reporter_plus_events) + '\n')
    sys.stdout.write(f"{count+1}-{count + 10} of {len(users)}: \n")
    sys.stdout.flush()
    debug_output = []

    for user in users:
        """
        For each user, look to see 
        """
        count = count + 1
        sys.stdout.write('.')
        if count % 10 == 0:
            sys.stdout.flush()
            print()
            sys.stdout.write(f"{count+1}-{count + 10} of {len(users)}: \n")

        # Look for any ignored user and move to the next user
        if user.username in ignored_users:
            eligible_for_ignorance.append(user.username)
            debug_output.append(f'User {user.username} is an ignored user.')
            continue

        if debug and count > DEBUG_COUNT:
            debug_output.append(f"DEBUG MODE: Count above {DEBUG_COUNT}")
            break

        if top_level_namespace_id:
            events = gl.users.get(user.id).events.list(as_list=False)
            projects = []
        else:
            events = user.events.list(as_list=False)
            projects = user.projects.list(as_list=False)
        

        if count == 1:
             sys.stdout.write(f'Beginning evaluation of first user\n')
             sys.stdout.flush()

        if not events:
            eligible_for_removal.append(user.username)
            eligible_for_removal_id.append(user.id)
            for project in projects:
                if project.visibility == 'private':
                    private_project_caveats = private_project_caveats + 1
                    break
            debug_output.append(f'There are no events for {user.username}')
            continue
        
        guest_only_events = True
        user_actions = []
        user_action_count = 0

        if count == 1:
             sys.stdout.write(f'Starting to evaluate all events for first user.\n')
             sys.stdout.flush()

        for action in events:
            try:
                if action.project_id not in all_project_ids_in_namespace:
                    continue
            except AttributeError:
                continue
            user_action_count += 1
            if action.action_name in reporter_plus_events:  # then not a valid guest...
                guest_only_events = False
                break
            elif user_action_count > max_actions_considered:
                debug_output.append('Max actions have been considered for user.')
                break
            elif action.action_name in user_actions:
                debug_output.append(f'Already seen {action.action_name} for user {user.username}')
                continue
            user_actions.append(action.action_name)
            all_event_actions.add(action.action_name)

        user_action_counts.append(user_action_count)

        if count == 1:
             sys.stdout.write(f'Completed evaluating all events for first user\n')
             sys.stdout.flush()
    
        if guest_only_events:
            # This only works for Self-Managed at this time.
            for project in projects:
                if project.visibility == 'private':
                    private_project_caveats = private_project_caveats + 1
                    break
            eligible_for_guest.append(user.username)
            eligible_for_guest_id.append(user.id)
        
        sys.stdout.write(f'{"".join(["." for x in range(count)])}\n')
        sys.stdout.flush()

    print()
    print('Eligible for Removal:', len(eligible_for_removal))
    print('Eligible for Guest Access:', len(eligible_for_guest))
    print('Total Number of Ultimate Licenses to Pay for:', len(users)-len(eligible_for_guest)-len(eligible_for_removal))
    debug_output.append('Action Counts:' + ', '.join([str(x) for x in user_action_counts]))
    print()
    print()
    print(f'* Please note that of the eligible users, {private_project_caveats} of them had access to private projects.')
    print(f'  If granted guest access, they would no longer be able to see the source code of those projects.')
    print()
    print(f'  This is a tool for instructional purposes only - please consult your sales partner at GitLab for details.')

    print(eligible_for_guest_id)

    with open('output.json', 'w') as f:
        f.write(json.dumps(dict(
            eligible_for_guest=eligible_for_guest,
            eligible_for_removal=eligible_for_removal,
        )))

    with open('output_id.json', 'w') as f:
        f.write(json.dumps(dict(
            eligible_for_guest=eligible_for_guest_id,
            eligible_for_removal=eligible_for_removal_id,
        )))

    if debug_output and debug:
        print('\n'.join(debug_output))
    
    if all_event_actions and debug:
        print('All events seen:', ', '.join(list(all_event_actions)))


if __name__ == "__main__":
    main()
